
class Board
  
  attr_accessor :grid, :winning_pos
  
  def initialize(grid = empty_array)
    @grid = grid 
  end 
  
  def place_mark(coords, mark)
    self[coords] = mark 
  end 
  
  def place_marks(coord_array, mark)
    coord_array.each {|coord| place_mark(coord, mark)}
  end 
  
  def winner
    @@winning_pos.each do |winning_combo|
      sample = self[winning_combo[0]]
      next if sample == nil 
      return sample if winning_combo.all? {|position|  self[position] == sample}
    end 
    nil 
  end 
  
  def over? 
    return true if winner || full?
    false 
  end 
  
  def open_spots
    spots = []
    grid.each.with_index do |row, row_index|
      row.each.with_index do |item, col_index|
        if empty?([row_index, col_index])
          spots << [row_index, col_index] 
        end 
      end 
    end 
    spots
  end 
  
  
  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
  
  def empty?(coords)
    self[coords] == nil
  end 
  

  
     @@winning_pos =
    [
     #Horizontal
     [[0, 0], [0, 1], [0, 2]],
     [[1, 0], [1, 1], [1, 2]],
     [[2, 0], [2, 1], [2, 2]],
     #Vertical
     [[0, 0], [1, 0], [2, 0]],
     [[0, 1], [1, 1], [2, 1]],
     [[0, 2], [1, 2], [2, 2]],
     #Diagonal 
     [[0, 0], [1, 1], [2, 2]],
     [[0, 2], [1, 1], [2, 0]],
     ]
  private
  def empty_array
    Array.new(3) {Array.new(3)}
  end 
  
  def full? 
    grid.flatten.none?(&:nil?)
  end 
  
end
