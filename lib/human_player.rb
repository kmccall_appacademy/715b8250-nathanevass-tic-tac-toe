class HumanPlayer
  
  attr_accessor :name, :mark
  
  def initialize(name)
    @name = name
  end 
  
  def get_move
    puts "where would you like to move? Answer in the form, (x, y) without
    parens where ax is the row and y is the column."
    
    move = gets.chomp 
    row, column = move[0].to_i, move[-1].to_i 
    [row, column]
  end 
  
  def display(board)
    board.grid.each do |row|
      row.each do |sym| 
        sym.nil? ? print("-") : print(sym)
      end 
      puts ""
    end 
    puts ""
  end 
end
