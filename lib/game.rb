require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require "byebug"

class Game
  attr_accessor :board, :player_one, :player_two, :current_player
  
  def play 
    play_turn until board.over?
    handle_end 
  end 
  
  def initialize(player_one, player_two)
    @board = Board.new 
    @player_one, @player_two = player_one, player_two 
    player_one.mark = :X 
    player_two.mark = :O
    @current_player = @player_one 
  end 
  
  def switch_players!
    @current_player == player_one ?
      @current_player = @player_two : @current_player = @player_one  
  end   
  
  def play_turn
    display
    board.place_mark(move, mark)
    display 
    switch_players!
  end   
  
  private
  
  def move 
    current_player.get_move 
  end 
  
  def mark 
    current_player.mark
  end 
  
  def display
    current_player.display(board)
  end 
  
  def handle_end 
    if board.winner == nil 
      puts "Its a tie!"
    else 
      puts "The winner is #{board.winner}"
    end 
  end 
end


if $PROGRAM_NAME == __FILE__
  print "Whats your name: "
  name = gets.chomp
  human = HumanPlayer.new(name)
  comp = ComputerPlayer.new('Samuel')
  game = Game.new(human, comp)
  game.play
end
