class ComputerPlayer
  attr_accessor :name, :board, :mark 
  
  def initialize(name)
    @name = name 
  end 
  
  def mark=(mark)
    @mark = mark 
  end 
  
  def display(board)
    @board = board
  end 
  
  def get_move
    @@winning_pos.each do |combination|
      board_values = combination.map {|coord| board[coord]}
      next unless board_values.count(nil) == 1 
      open_spot = board_values.index(nil)
      return combination[open_spot] if (board_values.count(:X) == 2 ||
        board_values.count(:O) == 2)
    end 
    board.open_spots.sample
  end 
  
  @@winning_pos =
    [
     #Horizontal
     [[0, 0], [0, 1], [0, 2]],
     [[1, 0], [1, 1], [1, 2]],
     [[2, 0], [2, 1], [2, 2]],
     #Vertical
     [[0, 0], [1, 0], [2, 0]],
     [[0, 1], [1, 1], [2, 1]],
     [[0, 2], [1, 2], [2, 2]],
     #Diagonal 
     [[0, 0], [1, 1], [2, 2]],
     [[0, 2], [1, 1], [2, 0]],
     ]
end
